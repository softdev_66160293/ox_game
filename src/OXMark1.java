package com.mycompany.mavenproject1;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */


import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OXMark1 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Welcome to OX Game!!!");
        
        String Player = "X";
        int row = 3;
        int col = 3;
        String[][] table = new String[row][col];

        //make everything = (-)
        for (int i=0; i<=2; i++ ){
            for (int j=0; j<=2; j++ ){
                table[i][j]="-";
                System.out.print("- ");
            }
            System.out.println();
        }
        System.out.println();
        //start win or lose
        boolean GAMEOVER = false;
        while (!GAMEOVER){
            System.out.println("Turn " + Player + " : ");
            System.out.print("Please input(row,col): ");
            String start = scanner.nextLine();
            //check (number number)
            if (!start.matches("\\d \\d")) {
                System.out.println("sai mai took nakub Please input again");
                continue;
            }
            //split number number to rows and cols
            String[] parts = start.split(" ");
            int rows = Integer.parseInt(parts[0]);
            int cols = Integer.parseInt(parts[1]);
            
            if (rows >= 1 && rows<=3 && cols>=1 && cols<=3){
                if (table[rows-1][cols-1].equals("-")){
                    table[rows-1][cols-1] = Player;
                    for (int i=0; i<=2; i++ ){
                        for (int j=0; j<=2; j++ ){
                            System.out.print(table[i][j]+" ");
                        }
                        System.out.println();
                    }
                    //this time to check it's correct?????
                    for (int i=0; i<=2; i++) {
                        if (table[i][0].equals(Player) && table[i][1].equals(Player) && table[i][2].equals(Player)){
                        System.out.println(Player+" Win!!!");
                        GAMEOVER = true;

                        } else if (table[0][i].equals(Player) && table[1][i].equals(Player) && table[2][i].equals(Player)){
                        System.out.println(Player+" Win!!!");
                        GAMEOVER = true;
                        }
                    }
                    //check diagonal kub
                    if (table[0][0].equals(Player) && table[1][1].equals(Player) && table[2][2].equals(Player)){
                        System.out.println(Player+" Win!!!");
                        GAMEOVER = true;
                        
                        } else if (table[0][2].equals(Player) && table[1][1].equals(Player) && table[2][0].equals(Player)){
                        System.out.println(Player+" Win!!!");
                        GAMEOVER = true;
                        }
                    //check draw
                    boolean found = false;
                    for (int i=0; i<=2; i++ ){
                        for (int j=0; j<=2; j++ ){
                            if (table[i][j].equals("-")){
                                found = true;
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                    if (found == false && GAMEOVER == false){
                        System.out.println("DRAW!!!");
                        System.exit(0);
                    }
                        
                    //if game is not done...
                    if (GAMEOVER == false){
                        Player = (Player.equals("X")) ? "O" : "X";
                    }
                } else{
                    System.out.println("repeated! please try again kub");
                    System.out.println();
                    continue;
                }
            } 
            else{
                System.out.println("1-3 only kub! pls try again");
            }
        }
    }
}